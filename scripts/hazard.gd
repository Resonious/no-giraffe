extends Node2D

# Doin' it like this...
var player
var bear
var music_manager

var collided_with_bear = false
var collided_with_player = false

func start(music_manager, player, bear):
	self.music_manager = music_manager
	self.player = player
	self.bear = bear
	add_to_group("hazards")


func _process(delta):
	position.x -= music_manager.scroll_speed() * delta

	if position.x < -1500:
		queue_free()
		return

	if !collided_with_bear and check_collision(bear, 50):
		bear.jump()
		collided_with_bear = true

	if !collided_with_player and check_collision(player, 35):
		if player.hit_by_hazard():
			collided_with_player = true

func check_collision(node, distance):
	var my_pos = get_global_position()
	var node_pos = node.get_global_position()
	var dist = my_pos.x - node_pos.x

	return dist > 0 and dist < distance
