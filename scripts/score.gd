extends Node

var seconds = 0.0 setget set_seconds
var minutes = 0.0 setget set_minutes
var hours = 0.0

func _ready():
	pass

func _process(delta):
	pass

func reset():
	seconds = 0.0
	minutes = 0.0
	hours = 0.0

func set_to(other_score):
	seconds = other_score.seconds
	minutes = other_score.minutes
	hours = other_score.hours

func get_total():
	return hours + (minutes * 60.0) + (seconds * 3600)

func set_seconds(value):
	seconds = value
	while seconds > 60.0:
		seconds -= 60.0
		self.minutes += 1.0

func set_minutes(value):
	minutes = value
	while minutes > 60.0:
		minutes -= 60.0
		self.hours += 1.0
