extends Node2D

const Step = preload("music_manager.gd").Step

export(Texture) var left_tex
export(Texture) var right_tex

export var note_separation = 300
export var look_ahead = 10

onready var original_pos = position
var slide_duration = 0
var music_manager setget ,get_music_manager

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass
	
func _draw():
	# draw the next $look_ahead steps
	var index = self.music_manager.step_index + 1
	var steps = self.music_manager.step_values
	if len(steps) == 0: return
	var pos = Vector2(-128, -128)
	for i in range(index, index + look_ahead + 1):
		var step = steps[i % len(steps)]
		match step:
			Step.STEP_LEFT:
				draw_texture(left_tex, pos)
			Step.STEP_RIGHT:
				draw_texture(right_tex, pos)
		pos += Vector2(note_separation, 0)
	
func _on_music_manager_beat(unused_val):
	var bpm = self.music_manager.bpm()
	slide_duration = 60.0 / bpm
	position = original_pos + Vector2(note_separation, 0)
	update() # redraw

func _process(delta):
	if slide_duration > 0:
		position += (Vector2(-note_separation / slide_duration, 0) * delta)

func get_music_manager():
	if !music_manager:
		music_manager = get_node(@"../../music_manager")
		music_manager.connect("beat", self, "_on_music_manager_beat")
	return music_manager
