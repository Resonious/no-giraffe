extends AnimatedSprite

const Step = preload("music_manager.gd").Step

var onbeat = false
var hit = false
var current_beat_value = -1


func _ready():
	pass


func _process(delta):
	if Input.is_action_just_pressed("step_left"):
		if onbeat and current_beat_value == Step.STEP_LEFT:
			pressed("left")
		else:
			fail()
	if Input.is_action_just_pressed("step_right"):
		if onbeat and current_beat_value == Step.STEP_RIGHT:
			pressed("right")
		else:
			fail()


func pressed(animation):
	play(animation)
	onbeat = false
	hit = true


func fail():
	play("default")


func _on_music_manager_beat_begin(value):
	onbeat = true
	current_beat_value = value


func _on_music_manager_beat_end(value):
	if !hit:
		fail()

	onbeat = false
	hit = false
