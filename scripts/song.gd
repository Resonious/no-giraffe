extends AudioStreamPlayer

export var steps = "lr"
export var bpm = 100.0
export var beat_offset = 0
export var loop_step_index = 0
