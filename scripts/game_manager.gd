extends Node

signal game_over_freeze

export var hits_til_loss = 5
export var push_back_amount = 55

export(Color) var white_flash = Color(1, 1, 1, 1)
var transparent = Color(1, 1, 1, 0)

onready var score = get_node("/root/score")
onready var seconds_text = get_node("camera/seconds")
onready var minutes_text = get_node("camera/minutes")
onready var hours_text = get_node("camera/hours")

func _ready():
	score.reset()

func _process(delta):
	score.seconds += delta
	seconds_text.text = "%02d" % int(score.seconds)
	minutes_text.text = "%02d" % int(score.minutes)
	hours_text.text = "%02d" % int(score.hours)

func _on_player_failed(player):
	# Crash sound
	$crash_sound.stream.loop = false
	$crash_sound.play()

	# Switch music, game over if necessary
	var music_manager = get_node(@"../music_manager")
	match music_manager.current_song:
		@"phase1": music_manager.current_song = @"phase2"
		@"phase2": music_manager.current_song = @"phase3"
		@"phase3": music_manager.current_song = @"phase4"
		@"phase4": game_over()
		
	# Reset the difficulty up timer since we just changed the music
	$difficulty_up_timer.start()

	# Push player back
	var new_pos = player.position
	new_pos.x -= push_back_amount
	$tween.interpolate_property(player, "position", player.position, new_pos, 0.6, Tween.TRANS_QUAD, Tween.EASE_IN)

	# Flash screen
	$white_flash.color
	$tween.interpolate_property($white_flash, "color", white_flash, transparent, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN)

	# Remove hazards
	get_tree().call_group("hazards", "queue_free")

	# Start tweening
	if not $tween.is_active():
        $tween.start()

func game_over():
	get_tree().call_group("music", "stop")
	get_node("../bear").angry()
	get_tree().paused = true
	emit_signal("game_over_freeze")
	$jump_scare_timer.start()

func _on_jump_scare_timer_timeout():
	get_tree().paused = false
	get_tree().change_scene("res://scenes/game_over.tscn")


func _on_difficulty_up_timer_timeout():
	var music_manager = get_node(@"../music_manager")
	match music_manager.current_song:
		@"phase1": music_manager.current_song = @"phase2"
		@"phase2": music_manager.current_song = @"phase3"
		@"phase3": music_manager.current_song = @"phase4"
		@"phase4": pass
