extends ParallaxBackground

onready var music_manager = get_node(@"../music_manager")

func _process(delta):
	scroll_offset.x -= delta * music_manager.scroll_speed()


func _on_game_manager_game_over_freeze():
	$red_filter.visible = true
