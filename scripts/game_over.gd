extends Node

var growl_finished = false

func _input(event):
	if growl_finished:
		get_tree().change_scene("res://scenes/tutorial.tscn")

func _on_growl_finished():
	get_node("CanvasLayer2/AnimationPlayer").play("fadein")
	growl_finished = true
