extends Node2D

export var probability_per_beat = 0.05
export var min_beats_between_hazards = 4

export(PackedScene) var hazard1
export(PackedScene) var hazard2

onready var music_manager = get_node(@"../music_manager")
onready var player = get_node(@"../player")
onready var bear = get_node(@"../bear")

var hazard_types = []
var cooldown = 0

func _ready():
	# Lazy... Probably can export this array but it's late at night.
	hazard_types = [hazard1, hazard2]

func _on_music_manager_beat(unused_val):
	if cooldown > 0:
		cooldown -= 1
		return

	if randf() < probability_per_beat:
		var hazard = hazard_types[randi() % len(hazard_types)]
		var hazard_node = hazard.instance()

		add_child(hazard_node)
		hazard_node.start(music_manager, player, bear)

		cooldown = min_beats_between_hazards

