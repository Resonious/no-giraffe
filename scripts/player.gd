extends Node2D

const Step = preload("music_manager.gd").Step

signal failed(player)
signal hurt(player)
signal stepped(player, direction)
signal jumped(player)

export var invincibility_beats = 3
export var hit_points = 3

var onbeat = false
var hitbeat = false
var failed_this_beat = false
var current_beat_value = -1

onready var hp_remaining = hit_points
var invincibility_counter = 10
onready var last_step = Step.STEP_REST
var music_manager setget ,get_music_manager
var jumped = false

var time_since_last_pressed = 0.0


func _process(delta):
	if is_jumping():
		return
	time_since_last_pressed += delta

	if Input.is_action_just_pressed("step_left"):
		if is_on_beat():
			match last_step:
				Step.STEP_LEFT:  jump()
				Step.STEP_RIGHT, Step.STEP_REST:
					pressed("left", Step.STEP_LEFT)
					last_step = Step.STEP_LEFT
		else:
			fail()

	if Input.is_action_just_pressed("step_right"):
		if is_on_beat():
			match last_step:
				Step.STEP_RIGHT:  jump()
				Step.STEP_LEFT, Step.STEP_REST:
					pressed("right", Step.STEP_RIGHT)
					last_step = Step.STEP_RIGHT
		else:
			fail()


func pressed(animation, direction):
	$animation.play("bounce", -1, 4)
	$sprite.play(animation)
	onbeat = false
	hitbeat = true
	jumped = false
	time_since_last_pressed = 0.0
	self.invincibility_counter /= 2
	emit_signal("stepped", self, direction)


func jump():
	if jumped:
		fail()
		return

	var speed
	if self.music_manager:
		speed = 0.5 / music_manager.seconds_per_beat()
	else:
		speed = 1

	$animation.play("jump", -1, speed)
	jumped = true
	self.invincibility_counter = 0
	emit_signal("jumped", self)


func is_jumping():
	return $animation.current_animation == "jump"


func fail():
	if failed_this_beat:
		return
	else:
		failed_this_beat = true

	time_since_last_pressed = 0.0

	jumped = false
	$animation.play("quiver", -4, 4)
	$sprite.play("hurt")

	last_step = Step.STEP_REST

	if invincibility_counter > 0:
		self.invincibility_counter -= 1
		return

	$hit_sound.play()
	emit_signal("hurt", self)

	# Set invincibility
	self.invincibility_counter = invincibility_beats

	# Decrement hp...
	if hp_remaining > 0:
		hp_remaining -= 1
		return
	else:
		hp_remaining = hit_points

	# Signal away
	emit_signal("failed", self)


func hit_by_hazard():
	if is_jumping():
		return false
	else:
		fail()
		return true


func is_on_beat():
	# Okay we're gonna check that either a) it's been long enough
	# since last press, or b) we're within the beat frame defined
	# by music manager (that the bear follows)
	if onbeat: return true
	if !self.music_manager: return true

	var threshold = self.music_manager.seconds_per_beat() / 2.0
	return time_since_last_pressed > threshold


func _on_music_manager_beat_begin(value):
	onbeat = true
	current_beat_value = value


func _on_music_manager_beat_end(value):
	if !hitbeat and invincibility_counter > 0:
		self.invincibility_counter -= 1

	var threshold = get_music_manager().seconds_per_beat() * 1.5
	if time_since_last_pressed > threshold:
		fail()

	onbeat = false
	hitbeat = false
	failed_this_beat = false



func get_music_manager():
	if !music_manager:
		music_manager = get_node("../music_manager")
	return music_manager


func _on_music_manager_beat(value):
	pass
