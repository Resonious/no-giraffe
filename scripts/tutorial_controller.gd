extends Node

const Step = preload("music_manager.gd").Step

var step_count = 0
var step_distance = 80
var jump_step = 5
var done_step = 8
var original_player_pos

func _ready():
	update_display($player, 0)
	original_player_pos = $player.position


func _on_player_stepped(player, step):
	if step_count != jump_step:
		update_display(player, step)
		step_count += 1
		push_player_forward(player)

		if step_count == jump_step:
			update_display_to_match(player, step)
		elif step_count == done_step:
			gotogame()
	else:
		reset()


func _on_player_jumped(player):
	if step_count == jump_step:
		push_player_forward(player, 2.0, Tween.EASE_IN)
		step_count += 1
		update_display(player, player.last_step)


func _on_done_tween_tween_completed(object, key):
	get_tree().change_scene("res://scenes/important_message.tscn")


func gotogame():
	var black = $blackforeground/blackness
	var target = black.modulate
	target.a = 1.0
	$done_tween.interpolate_property(black, "modulate", black.modulate, target, 1.5, Tween.TRANS_QUAD, Tween.EASE_IN)
	$done_tween.start()


func reset():
	update_display($player, 0)
	$player.last_step = Step.STEP_REST
	step_count = 0

	# Push player back to start
	$player/hit_sound.play()
	$tween.interpolate_property($player, "position", $player.position, original_player_pos, 1.2, Tween.TRANS_QUAD, Tween.EASE_IN)
	# Start tweening
	if not $tween.is_active():
        $tween.start()

func push_player_forward(player, amount = 1.0, easer = Tween.EASE_OUT):
	var new_pos = player.position
	new_pos.x += step_distance * amount

	$tween.interpolate_property(player, "position", player.position, new_pos, 0.2, Tween.TRANS_QUAD, easer)
	# Start tweening
	if not $tween.is_active():
        $tween.start()


func update_display(player, step):
	match step:
		Step.STEP_REST, Step.STEP_LEFT:
			$step_left.hide()
			$step_right.show()
		Step.STEP_RIGHT:
			$step_left.show()
			$step_right.hide()

func update_display_to_match(player, step):
	match step:
		Step.STEP_REST, Step.STEP_LEFT:
			$step_left.show()
			$step_right.hide()
		Step.STEP_RIGHT:
			$step_left.hide()
			$step_right.show()
