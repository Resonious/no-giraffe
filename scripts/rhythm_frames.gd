extends AnimatedSprite

const Step = preload("music_manager.gd").Step


func _ready():
	pass


func _on_music_manager_beat(value):
	if value == Step.STEP_LEFT:
		play("left")
	elif value == Step.STEP_RIGHT:
		play("right")


func _on_music_manager_beat_begin(value):
	pass # TODO is this callback needed?

