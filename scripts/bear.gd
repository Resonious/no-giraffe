extends Node2D

const Step = preload("music_manager.gd").Step

var music_manager


func _ready():
	pass


func _on_music_manager_beat(value):
	if !music_manager:
		music_manager = get_node("../music_manager")

	if value == Step.STEP_LEFT:
		$sprite.frame = 0
		$sprite.play("left")
		if $animation.current_animation != "jump":
			$animation.play("bounce", -1, 4)
	elif value == Step.STEP_RIGHT:
		$sprite.frame = 0
		$sprite.play("right")
		if $animation.current_animation != "jump":
			$animation.play("bounce", -1, 4)


func _on_music_manager_beat_begin(value):
	pass # TODO is this callback needed?

func angry():
	$sprite.play("angry")

func jump():
	$animation.play("jump", -1, 0.5 / music_manager.seconds_per_beat())


func _on_player_hurt(player):
	$sprite.play("angry_f")
	$growl.stream.loop = false
	$growl.play()
