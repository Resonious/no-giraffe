extends Node

enum Step { STEP_REST, STEP_LEFT, STEP_RIGHT }

export var pre_beat_proportion = 0.2
export var post_beat_proportion = 0.35

signal beat_begin(value)
signal beat(value)
signal beat_end(value)

export(NodePath) var current_song setget set_current_song
var song

# List of step values..
var step_values = []

# Current step_values index
var step_index = 0

# Previous frame's beat timer
var last_beat_timer = 0.0

# True when "on" the beat (defined by the pre/post_beat_proportions)
var on_beat = false


func _ready():
	set_current_song(current_song)
	if song:
		song.play()
		parse_steps_string(song.steps)


func _process(delta):
	###############################################################
	# Set on_beat to true or false as we pass the thresholds.
	###############################################################
	if beat_timer() >= pre_beat_threshold() and last_beat_timer < pre_beat_threshold():
		# Beat timer past early threshold
		on_beat = true
		#print("== begin beat ", last_beat_timer, " ", beat_timer())
		emit_signal("beat_begin", step_values[step_index])

	if beat_timer() < last_beat_timer:
		# Beat timer past zero
		#print("BEAT!!! ", last_beat_timer, " ", beat_timer())
		emit_signal("beat", step_values[step_index])

	if beat_timer() >= post_beat_threshold() and last_beat_timer < post_beat_threshold():
		# Beat timer past late threshold
		on_beat = false
		#print("end beat ", last_beat_timer, " ", beat_timer())
		emit_signal("beat_end", step_values[step_index])

		step_index += 1
		if step_index >= len(step_values):
			step_index = song.loop_step_index

	###############################################################
	# This has to happen each frame (keep track of previous frame beat timer)
	###############################################################
	last_beat_timer = beat_timer()


func scroll_speed():
	if song:
		return song.bpm # fuck it, right?
	else:
		return 0


func bpm():
	if song:
		return song.bpm
	else:
		return 0


# The almighty beat timer
func beat_timer():
	if !song:
		return 0

	var pos = song.get_playback_position() + song.beat_offset

	if pos:
		return fmod(pos, seconds_per_beat())
	else:
		return 0


func seconds_per_beat():
	if !song: return 0
	return 60.0 / bpm()


# As soon as the beat timer passes this threshold, we become "on" the beat
func pre_beat_threshold():
	# This is the late beat duration before the end of the beat.
	return seconds_per_beat() - pre_beat_duration()


# As soon as the beat timer passes this threshold, we're no longer "on" the beat
func post_beat_threshold():
	# This is actually 0 + the early beat duration -- it's after the last beat
	# ended.
	return post_beat_duration()


# The number of seconds for which we're "on" a beat
func pre_beat_duration():
	return pre_beat_proportion * seconds_per_beat()
func post_beat_duration():
	return post_beat_proportion * seconds_per_beat()


func set_current_song(value):
	current_song = value
	if !value:
		song = null
		return

	var playback_pos = 0
	var old_length = 0
	if song:
		playback_pos = song.get_playback_position()
		old_length = song.stream.get_length()
		song.stop()
	else:
		last_beat_timer = 0.0

	song = get_node(value)
	if !song:
		return

	parse_steps_string(song.steps)
	step_index = 0

	var new_playback_pos = 0.0
	if playback_pos != 0:
		new_playback_pos = playback_pos * (song.stream.get_length() / old_length)

	song.play(new_playback_pos)
	last_beat_timer = beat_timer()


func parse_steps_string(string):
	step_values = []
	for c in string:
		if c == 'l':
			step_values.append(STEP_LEFT)
		elif c == 'r':
			step_values.append(STEP_RIGHT)
		elif c == '-':
			step_values.append(STEP_REST)
