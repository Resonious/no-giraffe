extends Node

onready var score = get_node("/root/score")
onready var best_score = get_node("/root/best_score")

onready var score_text = get_node("score")
onready var best_score_text = get_node("best")

func _ready():
	if score.get_total() > best_score.get_total():
		best_score.set_to(score)
	
	score_text.text = "%02d:%02d:%02d" % [int(score.hours), int(score.minutes), int(score.seconds)]
	best_score_text.text = "%02d:%02d:%02d" % [int(best_score.hours), int(best_score.minutes), int(best_score.seconds)]
